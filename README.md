# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

I was playing Elite:Dangerous one day and I realized that there is a lot of information I want to keep track of about all the different systems I visit. Such as what stations are there and what they have available. This project should be a convenient way to input information and view it for use in exploration, trading, ship purchasing, or anything else that it could be used for. 

Version: 0.0.1

### How do I get set up? ###

Doesn't exist yet, wouldn't recommend trying to run this application. Might release Cthulhu.

### Contribution guidelines ###

Contribute how you see fit. Just don't be rude about anything. What I would recommend is if you see an issue you can fix, then do so and let me know when you're done with a detailed explanation of the problem and how you fixed it.

### Who do I talk to? ###

The only real contact for this project would be me, Xavier. You can e-mail me here: sneakyatt2@gmail.com